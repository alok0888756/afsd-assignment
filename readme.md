# local setup

1 - unzip the folder and run the index.html using any browser

# All step

1 - created a directory.
2 - go to the that directory and run command git init (to initialize the folder as git)
3 - then added remote - git remote add origin https://gitlab.com/alok0888756/afsd-assignment.git
4 - created a file index.html and added some changes (touch index.html)
5 - create a feature_branch (git checkout -b feature_branch)
6 - added some changes and commit that changes (git add ., git commit -m "message")
7 - create a feature_branch2 (git checkout -b feature_branch2)
8 - added some changes and commit that changes (git add ., git commit -m "message") in the same line as in feature_branch
9 - now checkout to master and merge feature_branch (git merge feature_branch)
10 - then when I try to merge the changes of feature_branch2 it shows conflict (screenshots/Screenshot from 2023-12-13 22-45-42.png)
11 - resolve the conflict and push changes it gitlab
